const bigInt = require('big-integer')

const lib = {

  getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min)) + min
  },

  genString (p = {}) {
    const params = { length: 10, useUpperCase: false, useDigits: false, useSymbols: false, ...p }
    let letters = 'qwertyuiopasdfghjklzxcvbnm'
    const numbers = '1234567890'
    const symbols = `\`!@#$%^&*(_+|~[]{};':",.<>/?\\`
    if (params.useUpperCase) {
      letters += letters.toUpperCase()
    }
    if (params.useDigits) {
      letters += numbers
    }
    if (params.useSymbols) {
      letters += symbols
    }
    let str = ''
    for (let i = 0; i < params.length; i++) {
      str += letters[this.getRandomInt(0, letters.length)]
    }
    return str
  },

  leadZero (str, length) {
    while (str.length < length) {
      str = '0' + str
    }
    return str
  },

  genLongByteNumber (b) {
    let str = ''
    for (let i = 0; i < b; i++) {
      str = str + this.leadZero(this.getRandomInt(0, 255).toString(16), 2)
    }
    return bigInt(str, 16)
  },

  strftime (date, str) {
    let ret = str
    const mask = {}
    mask['%y'] = this.leadZero(date.getFullYear().toString(), 2)
    mask['%m'] = this.leadZero((date.getMonth() + 1).toString(), 2)
    mask['%d'] = this.leadZero(date.getDate().toString(), 2)
    mask['%H'] = this.leadZero(date.getHours().toString(), 2)
    mask['%M'] = this.leadZero(date.getMinutes().toString(), 2)
    mask['%S'] = this.leadZero(date.getSeconds().toString(), 2)
    mask['%f'] = this.leadZero(date.getMilliseconds().toString(), 3)
    mask['%z'] = this.leadZero((date.getTimezoneOffset() / 60).toString(), 2)
    const keys = Object.keys(mask)
    for (const i in keys) {
      while (true) {
        if (ret.match(keys[i])) {
          ret = ret.replace(keys[i], mask[keys[i]])
        } else {
          break
        }
      }
    }
    return ret
  }

}

export default lib
